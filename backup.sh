#!/bin/sh

configFolder="${HOME}/.config"

copyConfigFile() {
  cp -rf "${configFolder}/${1}" "${2:-.}" 2>/dev/null && printf '%s\n' " - ${1}"
}

copyHomeDotFile() {
  cp -rf "${HOME}/${1}" "${2:-.}" 2>/dev/null && printf '%s\n' " - ${1}"
}

printf '%s\n' "Starting copy config files from ${configFolder}"
copyConfigFile "alacritty"
copyConfigFile "bspwm"
copyConfigFile "conky"
copyConfigFile "i3"
copyConfigFile "i3blocks"
copyConfigFile "i3lock"
copyConfigFile "kitty"
copyConfigFile "lf"
copyConfigFile "nvim"
copyConfigFile "neofetch"
copyConfigFile "polybar"
copyConfigFile "rofi"
copyConfigFile "sxhkd"

printf '\n'
printf '%s\n' "Starting copy dotfiles from ${HOME}"
copyHomeDotFile ".bashrc"
copyHomeDotFile ".bash_aliases"
copyHomeDotFile ".zshrc"

mkdir -p ".local"
copyHomeDotFile ".local/bin" ".local/"

mkdir -p "intellij"
copyHomeDotFile ".ideavimrc" "intellij"
copyConfigFile "JetBrains/IntelliJIdea2024.1/keymaps/Vim.xml" "intellij"
