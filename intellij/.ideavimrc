""" Config
set clipboard+=unnamedplus
set relativenumber number
set ideajoin
set idearefactormode=keep
set incsearch
set NERDTree
let mapleader=" "
let g:NERDTreeMapActivateNode='l'
let g:NERDTreeMapJumpParent='h'
""" Movement
sethandler <c-j> n-v:vim i:ide
sethandler <c-k> n-v:vim i:ide
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <S-h> :action PreviousTab<CR>
nnoremap <S-l> :action NextTab<CR>
""" Remove char, paste replace selected without register
nnoremap x "_x
vnoremap p "_dP
""" Editing source code
vnoremap < <gv
vnoremap > >gv
nnoremap [[ :action MethodUp<CR>
nnoremap ]] :action MethodDown<CR>
nnoremap zc :action CollapseRegion<CR>
nnoremap zo :action ExpandRegion<CR>
nnoremap zC :action CollapseRegionRecursively<CR>
nnoremap zO :action ExpandRegionRecursively<CR>
nnoremap z- :action CollapseAllRegions<CR>
nnoremap z= :action ExpandAllRegions<CR>
""" Leader mapping
nnoremap <leader>e :NERDTreeToggle<CR>
nnoremap <leader>ve :e ~/.ideavimrc<CR>
nnoremap <leader>vr :action IdeaVim.ReloadVimRc.reload<CR>
nnoremap <leader>r :action Refactorings.QuickListPopupAction<CR>
nnoremap <Leader>= :action ReformatCode<CR>
nnoremap <leader>o :action OptimizeImports<CR>
nnoremap <leader>l :action RecentLocations<CR>
nnoremap <leader>h :action LocalHistory.ShowHistory<CR>
nnoremap <leader>g :action Vcs.ShowTabbedFileHistory<CR>
nnoremap <leader>w :action JumpToLastWindow<CR>
nnoremap <leader>q :action HideActiveWindow<CR>
nnoremap <leader>T :action ActivateTerminalToolWindow<CR>
nnoremap <leader>t :action Terminal.OpenInTerminal<CR>
nnoremap <leader><leader> :action SearchEverywhere<CR>
nnoremap <leader>a :action GotoAction<CR>
nnoremap <leader>f :action GotoFile<CR>
nnoremap <leader>c :action GotoClass<CR>
nnoremap <leader>s :action GotoSymbol<CR>
nnoremap <leader>d :action GotoRelated<CR>
nnoremap <leader>x :action NewScratchFile<CR>
nnoremap <leader>p :action $LRU<CR>
nnoremap <leader>\ :action ShowSettings<CR>
"""Code actions
nnoremap ga :action ShowIntentionActions<CR>
nnoremap gs :action Generate<CR>
nnoremap gS :action SurroundWith<CR>
vnoremap gS :action SurroundWith<CR>
nnoremap gc :action IntroduceConstant<CR>
vnoremap gc :action IntroduceConstant<CR>
nnoremap gf :action RecentFiles<CR>
nnoremap gi :action GotoImplementation<CR>
nnoremap gd :action GotoDeclaration<CR>
nnoremap gp :action GotoSuperMethod<CR>
nnoremap gt :action GotoTest<CR>
nnoremap ge :action GotoNextError<CR>
nnoremap gE :action GotoPreviousError<CR>
nnoremap gr :action RenameElement<CR>
nnoremap gn :action Inline<CR>
nnoremap gm :action ExtractMethod<CR>
vnoremap gm :action ExtractMethod<CR>
nnoremap gv :action IntroduceVariable<CR>
vnoremap gv :action IntroduceVariable<CR>
nnoremap gu :action FindUsages<CR>
vnoremap g/ :action CommentByLineComment<CR>
nnoremap g/ :action CommentByLineComment<CR>
nnoremap <c-f> :action FindInPath<CR>
nnoremap <c-o> :action Back<CR>
nnoremap <c-i> :action Forward<CR>
nnoremap <c-r> :action $Redo<CR>
nnoremap <c-s> :action FileStructurePopup<CR>
inoremap <c-p> :action ParameterInfo<CR>
nnoremap <c-x> :action TogglePresentationMode<CR>
nnoremap <c-z> :action ToggleDistractionFreeMode<CR>
nnoremap <c-m> :action MoveEditorToOppositeTabGroup<CR>
nnoremap <c-S-k> :action QuickImplementations<CR>
vnoremap <a-j> :action MoveStatementDown<CR>
nnoremap <a-j> :action MoveStatementDown<CR>
vnoremap <a-k> :action MoveStatementUp<CR>
nnoremap <a-k> :action MoveStatementUp<CR>
vnoremap <a-S-j> :action SelectNextOccurrence<CR>
nnoremap <a-S-j> :action SelectNextOccurrence<CR>
vnoremap <a-c-s-j> :action SelectAllOccurrences<CR>
nnoremap <a-c-s-j> :action SelectAllOccurrences<CR>
""" Running and Debugging
nnoremap ,r :action RunClass<CR>
nnoremap ,d :action DebugClass<CR>
nnoremap ,u :action Rerun<CR>
nnoremap ,f :action RerunFailedTests<CR>
nnoremap ,b :action ToggleLineBreakpoint<CR>
nnoremap ,s :action Stop<CR>
""" Harpoon
nnoremap ’ :action ShowHarpoon<cr>
nnoremap ,, :action AddToHarpoon<cr>
nnoremap ,≠ :action SetHarpoon1<cr>
nnoremap ,² :action SetHarpoon2<cr>
nnoremap ,³ :action SetHarpoon3<cr>
nnoremap ,¢ :action SetHarpoon4<cr>
nnoremap ,€ :action SetHarpoon5<cr>
nnoremap ≠ :action GotoHarpoon1<cr>
nnoremap ² :action GotoHarpoon2<cr>
nnoremap ³ :action GotoHarpoon3<cr>
nnoremap ¢ :action GotoHarpoon4<cr>
nnoremap € :action GoToHarpoon5<cr>
