-- Highlight on yank
vim.api.nvim_create_autocmd("TextYankPost", {
  callback = function()
    vim.highlight.on_yank({ on_visual = false })
  end
})

-- Disable comment new line
vim.api.nvim_create_autocmd("BufWinEnter", {
  callback = function()
    vim.opt_local.formatoptions:remove { "c", "r", "o" }
  end,
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = { "gitcommit", "markdown" },
  callback = function()
    vim.opt_local.wrap = true
    vim.opt_local.spell = true
  end,
})

-- Automatically close the quickfix list
vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = "qf",
  callback = function()
    local opts = { noremap = true, silent = true }
    vim.api.nvim_set_keymap("n", "<CR>", "<CR>:cclose<CR>", opts)
    vim.api.nvim_set_keymap("n", "<ESC>", ":cclose<CR>", opts)
  end
})

-- Automatically close when NvimTree_ is last window
vim.api.nvim_create_autocmd("BufEnter", {
  nested = true,
  callback = function()
    if #vim.api.nvim_list_wins() == 1 and vim.api.nvim_buf_get_name(0):match("NvimTree_") ~= nil then
      vim.cmd "quit"
    end
  end
})
