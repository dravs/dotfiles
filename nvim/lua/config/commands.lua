local M = {}

vim.api.nvim_create_user_command("Config", function() vim.cmd([[cd ~/.config/nvim]]) end, {})

vim.api.nvim_create_user_command("Build", function()
  if M.fileInCwd('pom.xml') then
    if M.fileInCwd('mvnw') then
      M.runTermCmd("./mvnw clean package")
    else
      M.runTermCmd("mvn clean package")
    end
  elseif M.fileInCwd("build.gradle") then
    if M.fileInCwd("gradlew") then
      M.runTermCmd("./gradlew build")
    else
      M.runTermCmd("gradle build")
    end
  end
end, {})

vim.api.nvim_create_user_command("TestAll", function()
  if M.fileInCwd('pom.xml') then
    if M.fileInCwd('mvnw') then
      M.runTermCmd("./mvnw test")
    else
      M.runTermCmd("mvn test")
    end
  elseif M.fileInCwd("build.gradle") then
    if M.fileInCwd("gradlew") then
      M.runTermCmd("./gradlew test")
    else
      M.runTermCmd("gradle test")
    end
  end
end, {})

vim.api.nvim_create_user_command("Test", function()
  local filename = vim.fn.expand('%:t')
  if M.fileInCwd('pom.xml') then
    if M.fileInCwd('mvnw') then
      M.runTermCmd("./mvnw test -Dtest=" .. ("'%s'"):format(filename))
    else
      M.runTermCmd("mvn test -Dtest=" .. ("'%s'"):format(filename))
    end
  elseif M.fileInCwd("build.gradle") then
    if M.fileInCwd("gradlew") then
      M.runTermCmd("./gradlew test --tests " .. ("'%s'"):format(filename))
    else
      M.runTermCmd("gradle test --tests " .. ("'%s'"):format(filename))
    end
  end
end, {})

function M.runTermCmd(command)
  vim.cmd("sp")
  vim.cmd("term " .. command)
  vim.cmd("resize 20N")
  local keys = vim.api.nvim_replace_termcodes("i", true, false, true)
  vim.api.nvim_feedkeys(keys, "n", false)
end

function M.fileInCwd(nameToCheck)
    local cwDir = vim.fn.getcwd()
    local cwdContent = vim.split(vim.fn.glob(cwDir .. "/*"), '\n', {trimempty=true})
    local fullNameToCheck = cwDir .. "/" .. nameToCheck
    for _, cwdItem in pairs(cwdContent) do
        if cwdItem == fullNameToCheck then
            return true
        end
    end
    return false
end
