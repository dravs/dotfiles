local opts = { noremap = true, silent = true }

vim.api.nvim_set_keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = [[ ]]
vim.g.maplocalleader = [[ ]]

-- Better window navigation
vim.api.nvim_set_keymap("n", "<C-h>", "<C-w>h", opts)
vim.api.nvim_set_keymap("n", "<C-j>", "<C-w>j", opts)
vim.api.nvim_set_keymap("n", "<C-k>", "<C-w>k", opts)
vim.api.nvim_set_keymap("n", "<C-l>", "<C-w>l", opts)

-- Resize with arrows
vim.api.nvim_set_keymap("n", "<C-Up>", ":resize -2<CR>", opts)
vim.api.nvim_set_keymap("n", "<C-Down>", ":resize +2<CR>", opts)
vim.api.nvim_set_keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
vim.api.nvim_set_keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Naviagate buffers
vim.api.nvim_set_keymap("n", "<S-l>", ":bnext<CR>", opts)
vim.api.nvim_set_keymap("n", "<S-h>", ":bprevious<CR>", opts)

-- Stay in indent mode
vim.api.nvim_set_keymap("v", "<", "<gv", opts)
vim.api.nvim_set_keymap("v", ">", ">gv", opts)

-- Move text up and down
vim.api.nvim_set_keymap("v", "<A-j>", ":m .+1<CR>==", opts)
vim.api.nvim_set_keymap("v", "<A-k>", ":m .-2<CR>==", opts)

-- Remove char, paste replace selected without register
vim.api.nvim_set_keymap("n", "x", '"_x', opts)
vim.api.nvim_set_keymap("v", "p", '"_dP', opts)

-- Dap
vim.keymap.set("n", "<F5>", function() require('dap').toggle_breakpoint() end)
vim.keymap.set("n", "<F9>", function() require('dap').continue() end)
vim.keymap.set("n", "<F10>", function() require('dap').run_to_cursor() end)

vim.keymap.set("n", "<F6>", function() require('dap').step_out() end)
vim.keymap.set("n", "<F7>", function() require('dap').step_into() end)
vim.keymap.set("n", "<F8>", function() require('dap').step_over() end)
