local cmd = vim.cmd
local opt = vim.opt
local g = vim.g

cmd [[ syntax on filetype plugin indent on ]]

local options = {
  autoindent = true,
  backup = false,
  breakindent = true,
  clipboard = "unnamed,unnamedplus",
  cmdheight = 2,
  completeopt = { "menuone", "noselect" },
  conceallevel = 0,
  cursorline = true,
  encoding = "utf-8",
  expandtab = true,
  fileencoding = "utf-8",
  hlsearch = true,
  hidden = true,
  incsearch = true,
  ignorecase = true,
  laststatus = 3,
  list = true,
  number = true,
  numberwidth = 4,
  mouse = 'a',
  pumheight = 10,
  redrawtime = 10000,
  relativenumber = true,
  scrolloff = 8,
  sidescrolloff = 8,
  signcolumn = "yes:1",
  shiftwidth = 2,
  showmode = false,
  smartcase = true,
  smartindent = true,
  softtabstop = 2,
  splitbelow = true,
  splitright = true,
  swapfile = false,
  tabstop = 2,
  termguicolors = true,
  timeoutlen = 200,
  title = true,
  undofile = true,
  updatetime = 300,
  wrap = false,
}

for k, v in pairs(options) do
  opt[k] = v
end

opt.shortmess:append "c"
opt.wildignore:append { '*.DS_Store' }
opt.whichwrap:append { ['<'] = true, ['>'] = true, ['h'] = true, ['l'] = true, ['['] = true, [']'] = true }
opt.iskeyword:append { '@-@', '-', '$' }
opt.listchars:append { eol = '↲', trail = '·', tab = '▸ ', precedes = '«',extends = '»' }

g.loaded_ruby_provider = 0
g.loaded_perl_provider = 0
