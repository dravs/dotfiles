local modes = {
  ["n"] = "NORMAL",
  ["no"] = "NORMAL",
  ["v"] = "VISUAL",
  ["V"] = "V-LINE",
  [""] = "V-BLOCK",
  ["s"] = "SELECT",
  ["S"] = "S-LINE",
  [""] = "S-BLOCK",
  ["i"] = "INSERT",
  ["ic"] = "INSERT",
  ["R"] = "REPLACE",
  ["Rv"] = "V-REPLACE",
  ["c"] = "COMMAND",
  ["cv"] = "VIM EX",
  ["ce"] = "EX",
  ["r"] = "PROMPT",
  ["rm"] = "MOAR",
  ["r?"] = "CONFIRM",
  ["!"] = "SHELL",
  ["t"] = "TERMINAL",
}

local modes_colors = {
   "StatuslineAccent",
   "StatuslineAccentNC",
   "StatuslineInsertAccent",
   "StatuslineVisualAccent",
   "StatuslineReplaceAccent",
   "StatuslineCmdLineAccent",
   "StatuslineTerminalAccent"
}

for _, mode_color in ipairs(modes_colors) do
  local hl = vim.api.nvim_get_hl(0, {name = mode_color})
   if not hl then
       return
   end
   local color_name = mode_color .. "Separator"
   vim.api.nvim_set_hl(0, color_name, {fg = hl.bg})
end

local function mode()
  local current_mode = vim.api.nvim_get_mode().mode
  return string.format(" %s ", modes[current_mode]):upper()
end

local function get_mode_color()
  local current_mode = vim.api.nvim_get_mode().mode
  local color = "StatuslineAccent"
  if current_mode == "n" then
      color = "StatuslineAccent"
  elseif current_mode == "i" or current_mode == "ic" then
      color = "StatuslineInsertAccent"
  elseif current_mode == "v" or current_mode == "V" or current_mode == "" then
      color = "StatuslineVisualAccent"
  elseif current_mode == "R" then
      color = "StatuslineReplaceAccent"
  elseif current_mode == "c" then
      color = "StatuslineCmdLineAccent"
  elseif current_mode == "t" then
      color = "StatuslineTerminalAccent"
  end
  return color
end

local function mode_color()
  return '%#' .. get_mode_color() .. '#'
end

local function separator_mode_color()
  return '%#' .. get_mode_color().. 'Separator#'
end

local function cwd()
  local dir = vim.fn.fnamemodify(vim.fn.getcwd(), ':~')
  return string.format(" %s ", dir)
end

local function filepath()
  local fpath = vim.fn.fnamemodify(vim.fn.expand "%", ":~:.:h")
  if fpath == "" or fpath == "." then
      return " "
  end

  return string.format(" %%<%s/", fpath)
end

local function filename()
  local fname = vim.fn.expand "%:t"
  return fname .. " "
end

local function lsp()
  local count = {}
  local levels = {
    errors = vim.diagnostic.severity.ERROR,
    warnings = vim.diagnostic.severity.WARN,
    info = vim.diagnostic.severity.INFO,
    hints = vim.diagnostic.severity.HINT
  }


  for k, level in pairs(levels) do
    count[k] = vim.tbl_count(vim.diagnostic.get(0, { severity = level }))
  end

  local errors = ""
  local warnings = ""
  local hints = ""
  local info = ""

  if count["errors"] ~= 0 then
    errors = " %#LspDiagnosticsSignError# " .. count["errors"]
  end
  if count["warnings"] ~= 0 then
    warnings = " %#LspDiagnosticsSignWarning# " .. count["warnings"]
  end
  if count["hints"] ~= 0 then
    hints = " %#LspDiagnosticsSignHint# " .. count["hints"]
  end
  if count["info"] ~= 0 then
    info = " %#LspDiagnosticsSignInformation# " .. count["info"]
  end

  return errors .. warnings .. hints .. info .. "%#StatuslineNC#"
end

local vcs = function()
  local git_info = vim.b.gitsigns_status_dict
  if not git_info or git_info.head == "" then
    return ""
  end
  local added = git_info.added and ("%#GitSignsAdd#+" .. git_info.added .. " ") or ""
  local changed = git_info.changed and ("%#GitSignsChange#~" .. git_info.changed .. " ") or ""
  local removed = git_info.removed and ("%#GitSignsDelete#-" .. git_info.removed .. " ") or ""
  if git_info.added == 0 then
    added = ""
  end
  if git_info.changed == 0 then
    changed = ""
  end
  if git_info.removed == 0 then
    removed = ""
  end
  return table.concat {
     "%#GitSignsAdd#  ",
     git_info.head,
     " ",
     added,
     changed,
     removed,
     " ",
     " %#StatuslineNC#",
  }
end

local function filetype()
  return string.format(" %s ", vim.bo.filetype)
end

local function lineinfo()
  return " %l:%c %P "
end

local spaces = function()
  return "spaces: " .. vim.api.nvim_get_option_value("shiftwidth", {})
end

local recording = function()
  if vim.fn.reg_recording() ~= '' then
    return 'recording @' .. vim.fn.reg_recording()
  else
    return ''
  end
end

Statusline = {}

Statusline.active = function()
  return table.concat {
    mode_color(),
    mode(),
    separator_mode_color(),
    '',
    "%#StatuslineNC#",
    vcs(),
    filepath(),
    filename(),
    lsp(),
    "%=",
    recording(),
    spaces(),
    filetype(),
    separator_mode_color(),
    "",
    mode_color(),
    lineinfo(),
  }
end

function Statusline.inactive()
  return " %F"
end

function Statusline.neotree()
  return table.concat {
    mode_color(),
    ' F-TREE ',
    separator_mode_color(),
    '',
    "%#StatuslineNC#",
    cwd(),
    "%=",
    "   ",
    separator_mode_color(),
    "",
    mode_color(),
    ' NeoTree ',
  }
end

local group = vim.api.nvim_create_augroup("Statusline", { clear = false })

vim.api.nvim_create_autocmd({ "WinEnter", "BufEnter" }, {
  group = group,
  callback = function(opts)
		if vim.bo[opts.buf].filetype == 'neo-tree' then
      vim.cmd "setlocal statusline=%!v:lua.Statusline.neotree()"
      return
		end
    vim.cmd "setlocal statusline=%!v:lua.Statusline.active()"
  end
})

vim.api.nvim_create_autocmd({ "WinLeave", "BufLeave" }, {
  group = group,
  callback = function()
    vim.cmd "setlocal statusline=%!v:lua.Statusline.inactive()"
  end
})
