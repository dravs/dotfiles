local group = vim.api.nvim_create_augroup("LSPAutogroup", { clear = false })

vim.api.nvim_create_autocmd({ "FileType" }, {
  group = group,
  pattern = "java",
  callback = function()
    require('lsp.config.java').setup()
  end
})

local M = {}

function M.setup()
  local on_attach = function(client, bufnr)
    require('lsp.handlers').on_attach(client, bufnr)
    require('jdtls').setup_dap({ hotcodereplace = 'auto' })
    require('lsp.projects').setup_dap_main_class_configs()
    require('jdtls.dap').setup_dap_main_class_configs()
  end

  local root_markers = { '.git', '.gitignore', 'mvnw', 'gradlew' }
  local root_dir = require('jdtls.setup').find_root(root_markers)
  local home = os.getenv('HOME')

  local capabilities = {
    workspace = {
      configuration = true
    },
    textDocument = {
      completion = {
        completionItem = {
          snippetSupport = true
        }
      }
    }
  }

  local workspace_folder = home .. "/.cache/nvim/workspace/" .. vim.fn.fnamemodify(root_dir, ":p:h:t")
  local config = {
    flags = {
      allow_incremental_sync = true,
    },
    capabilities = capabilities,
    on_attach = on_attach,
  }

  config.settings = {
    java = {
      eclipse = {
        downloadSources = true,
      },
      maven = {
        downloadSources = true,
      },
      inlayHints = { parameterNames = { enabled = "all" } },
      implementationsCodeLens = {
        enabled = false,
      },
      referencesCodeLens = {
        enabled = false,
      },
      references = {
        includeDecompiledSources = false,
      },
      format = {
        enabled = false,
      },
      saveActions = {
        organizeImports = true,
      },
      signatureHelp = { enabled = true },
      contentProvider = { preferred = 'fernflower' },
      completion = {
        favoriteStaticMembers = {
          "org.hamcrest.MatcherAssert.assertThat",
          "org.hamcrest.Matchers.*",
          "org.hamcrest.CoreMatchers.*",
          "org.junit.jupiter.api.Assertions.*",
          "java.util.Objects.requireNonNull",
          "java.util.Objects.requireNonNullElse",
          "org.mockito.Mockito.*"
        },
        importOrder = {
          "java",
          "javax",
          "com",
          "org"
        }
      },
      sources = {
        organizeImports = {
          starThreshold = 9999,
          staticStarThreshold = 9999,
        },
      },
      codeGeneration = {
        toString = {
          template = "${object.className}{${member.name()}=${member.value}, ${otherMembers}}"
        }
      },
      configuration = {
        updateBuildConfiguration = "interactive",
        runtimes = {
          {
            name = "JavaSE-11",
            path = "/usr/lib/jvm/java-11-openjdk/",
          },
          {
            name = "JavaSE-17",
            path = "/usr/lib/jvm/java-17-openjdk/",
          },
          {
            name = "JavaSE-22",
            path = "/usr/lib/jvm/java-22-jdk/",
          }
        }
      },
    },
  }
  config.cmd = { 'jdtls', '-data', workspace_folder }
  config.on_attach = on_attach
  config.on_init = function(client, _)
    client.notify('workspace/didChangeConfiguration', { settings = config.settings })
  end

  -- Bundles --
  -- java-debug
  -- git clone https://github.com/microsoft/java-debug.git && cd java-debug && ./mvnw clean install
  -- or
  -- download last version from mvn repo: https://repo1.maven.org/maven2/com/microsoft/java/com.microsoft.java.debug.plugin/
  --
  -- java-decompiler
  -- git clone https://github.com/dgileadi/vscode-java-decompiler.git
  --
  -- java-test
  -- git clone https://github.com/microsoft/vscode-java-test.git && cd vscode-java-test && npm install && npm run build-plugin

  local jar_patterns = {
    home .. '/.local/jars/java-debug/com.microsoft.java.debug.plugin/target/com.microsoft.java.debug.plugin-*.jar',
    home .. '/.local/jars/vscode-java-decompiler/server/*.jar',
    home .. '/.local/jars/vscode-java-test/server/*.jar',
  }

  local bundles = {}
  for _, jar_pattern in ipairs(jar_patterns) do
    for _, bundle in ipairs(vim.split(vim.fn.glob(jar_pattern), '\n')) do
      if not
          (
            vim.endswith(bundle, 'com.microsoft.java.test.runner.jar') or
            vim.endswith(bundle, 'com.microsoft.java.test.runner-jar-with-dependencies.jar')) then
        table.insert(bundles, bundle)
      end
    end
  end

  local extendedClientCapabilities = require 'jdtls'.extendedClientCapabilities
  extendedClientCapabilities.resolveAdditionalTextEditsSupport = true
  extendedClientCapabilities.onCompletionItemSelectedCommand = "editor.action.triggerParameterHints"
  config.init_options = {
    bundles = bundles,
    extendedClientCapabilities = extendedClientCapabilities,
  }

  -- Server
  require('jdtls').start_or_attach(config)
end

return M
