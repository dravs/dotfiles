local null_ls = require('null-ls')

local code_actions = null_ls.builtins.code_actions
local formatting = null_ls.builtins.formatting

local sources = {
  code_actions.gitsigns,
  formatting.xmllint,
  formatting.gofmt,
  formatting.goimports,
  formatting.google_java_format,
  formatting.prettier.with({ extra_args = { "--no-semi", "--single-quote", "--jsx-single-quote" } }),
  formatting.black.with({ extra_args = { "--fast" } }),
  formatting.shfmt.with({
    filetypes = { 'sh', 'zsh', 'bash' },
    args = { '-i', '2' },
  }),
}

null_ls.setup({
  sources = sources,
  on_attach = require('lsp.handlers').on_attach,
  update_on_insert = true,
})
