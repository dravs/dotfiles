local M = {}

local function lsp_keymaps(bufnr)
  local opts = { noremap = true, silent = true }

  vim.api.nvim_buf_set_keymap(bufnr, "n", "ga", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "<C-k>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "gu", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "gr", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "[d", '<cmd>lua vim.diagnostic.goto_prev({ border = "rounded" })<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "gl",
    '<cmd>lua vim.diagnostic.open_float({scope="line"}))<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "]d", '<cmd>lua vim.diagnostic.goto_next({ border = "rounded" })<CR>', opts)
  vim.cmd [[ command! Format execute 'lua vim.lsp.buf.format({ async = false })' ]]
end

local function java_specific_keymaps(bufnr)
  local opts = { noremap = true, silent = true }
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'go', '<Cmd>lua require("jdtls").organize_imports()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gC', '<Cmd>lua require("jdtls").extract_constant()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'v', 'gC', '<Esc><Cmd>lua require("jdtls").extract_constant(true)<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gv', '<Cmd>lua require("jdtls").extract_variable()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'v', 'gv', '<Esc><Cmd>lua require("jdtls").extract_variable(true)<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'v', 'gm', '<Esc><Cmd>lua require("jdtls").extract_method(true)<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "gti", "<Cmd>lua require('jdtls.tests').goto_subjects()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "gtc", "<Cmd>lua require('jdtls').test_class()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "gtm", "<Cmd>lua require('jdtls').test_nearest_method()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>cti", "<Cmd>lua require('jdtls.tests').goto_subjects()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>ctc", "<Cmd>lua require('jdtls').test_class()<CR>", opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>ctm", "<Cmd>lua require('jdtls').test_nearest_method()<CR>", opts)

  vim.cmd [[ command! -buffer JdtCompile execute "lua require('jdtls').compile()" ]]
  vim.cmd [[ command! -buffer JdtUpdateConfig execute "lua require('jdtls').update_project_config()" ]]
  vim.cmd [[ command! -buffer JdtJol execute "lua require('jdtls').jol()" ]]
  vim.cmd [[ command! -buffer JdtBytecode execute "lua require('jdtls').javap()" ]]
  vim.cmd [[ command! -buffer JdtJshell execute "lua require('jdtls').jshell()" ]]
end

local function attach_navic(client, bufnr)
  vim.g.navic_silence = true
  local status_ok, navic = pcall(require, "nvim-navic")
  if not status_ok then
    return
  end
  navic.attach(client, bufnr)
end

M.on_attach = function(client, bufnr)
  attach_navic(client, bufnr)
  if client.name == "jdtls" or client.name == "jdt.ls" then
    java_specific_keymaps(bufnr)
    vim.lsp.codelens.refresh()
  end
  lsp_keymaps(bufnr)
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
M.capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

return M
