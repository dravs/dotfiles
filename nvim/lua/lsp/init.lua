local signs = {
  Error = ' ',
  Warning = ' ',
  Warn = ' ',
  Hint = ' ',
  Information = ' ',
  Info = ' ',
}

for type, icon in pairs(signs) do
  local highlight = 'DiagnosticSign' .. type
  vim.fn.sign_define(highlight, { text = icon, texthl = highlight, numhl = '' })
end

local config = {
  virtual_text = true,
  signs = true,
  update_in_insert = true,
  underline = true,
  severity_sort = true,
  float = {
    focusable = false,
    style = "minimal",
    border = "rounded",
    source = "always",
    header = "",
    prefix = "",
  },
}

vim.diagnostic.config(config)

require "lsp.config.lua"
require "lsp.config.java"
require "lsp.config.null-ls"
require "lsp.config.go"
