local M = {}

function M.setup_dap_main_class_configs()
  if vim.fn.getcwd() == '/home/dravs/Projects/hex' then
    require('dap').configurations.java = {
      {
        javaExec = "/usr/lib/jvm/java-21-jdk/bin/java",
        mainClass = "dev.dravs.hex.HexApplication",
        name = "Launch",
        projectName = "Hex",
        request = "launch",
        type = "java",
        cwd = "/home/dravs/Projects/hex/application"
      }
    }
  end
end

return M
