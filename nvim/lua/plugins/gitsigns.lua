return {
  'lewis6991/gitsigns.nvim',
  event = 'BufReadPre',
  dependencies = {
    'nvim-lua/plenary.nvim'
  },
  config = function()
    require('gitsigns').setup({
      signs = {
        add = { text = "▎" },
        change = { text = "▎" },
        delete = { text = "契" },
        topdelete = { text = "契" },
        changedelete = { text = "▎" }
      }
    })
  end,
}
