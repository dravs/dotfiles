return {
  'lewis6991/impatient.nvim',
  lazy = false,
  config = function()
    require('impatient').enable_profile()
  end,
}
