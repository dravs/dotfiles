return {
  'ethanholz/nvim-lastplace',
  lazy = false,
  config = function()
    require 'nvim-lastplace'.setup {}
  end
}
