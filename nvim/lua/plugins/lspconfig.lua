return {
  'neovim/nvim-lspconfig',
  event = 'BufReadPre',
  dependencies = {
    'mfussenegger/nvim-jdtls',
    'nvimtools/none-ls.nvim',
    'gbprod/none-ls-shellcheck.nvim',
  },
  config = function()
    require('lsp')
  end,
}
