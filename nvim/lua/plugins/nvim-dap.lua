return {
  'mfussenegger/nvim-dap',
  event = 'BufReadPre',
  dependencies = { "rcarriga/nvim-dap-ui", "nvim-neotest/nvim-nio", "theHamsta/nvim-dap-virtual-text", "leoluz/nvim-dap-go" },
  config = function()
    local dap = require('dap')
    dap.defaults.java.terminal_win_cmd = '20split new'
    dap.defaults.java.focus_terminal = true

    require('dapui').setup({
      mappings = {
        expand = { "o", "<2-LeftMouse>" },
        open = "<CR>",
        remove = "d",
        edit = "e",
        repl = "r",
        toggle = "t",
      },
      sidebar = {
        elements = {
          { id = "breakpoints", size = 0.25 },
          {
            id = "scopes",
            size = 0.50,
          },
          { id = "repl",        size = 0.25 },
        },
        size = 40,
        position = "right",
      },
      tray = {
        elements = {},
      },
    })

    require("nvim-dap-virtual-text").setup({
      show_stop_reason = false
    })

    vim.fn.sign_define('DapBreakpoint', {
      text = '',
      texthl = 'DapBreakpoint',
      linehl = 'CursorLine',
      numhl = 'DapBreakpoint',
    })

    vim.fn.sign_define('DapBreakpointCondition', {
      text = '',
      texthl = 'DapBreakpointCondition',
      linehl = 'CursorLine',
      numhl = 'DapBreakpointCondition',
    })

    vim.fn.sign_define('DapBreakpointRejected', {
      text = '',
      texthl = 'DapBreakpointRejected',
      linehl = '',
      numhl = 'DapBreakpointRejected',
    })

    vim.fn.sign_define('DapStopped', {
      text = '',
      texthl = 'DapStopped',
      linehl = 'DapStoppedCursorLine',
      numhl = 'DapStopped',
    })

    vim.fn.sign_define('DapLogPoint', {
      text = '﬌',
      texthl = 'DapLogPoint',
      linehl = '',
      numhl = 'DapLogPoint',
    })
    require('dap-go').setup()
  end
}
