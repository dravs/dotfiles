return {
  'rcarriga/nvim-notify',
  event = 'BufEnter',
  config = function()
    vim.notify = require('notify')
  end,
}
