return {
  'dr4vs/project.nvim',
  -- dir = '~/Projects/project.nvim',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'Shatur/neovim-session-manager',
  },
  event = 'VeryLazy',
  config = function()
    require("project_nvim").setup({
      detection_methods = { "lsp", "pattern" },
      patterns = { '.git', '.gitignore', 'mvnw', 'gradlew' },
      ignore_lsp = { "lua_ls", "null-ls" },
    })
  end,
}
