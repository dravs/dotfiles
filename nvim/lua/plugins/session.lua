return {
  'Shatur/neovim-session-manager',
  dependencies = {
    'nvim-lua/plenary.nvim'
  },
  event = 'VeryLazy',
  config = function()
    require('session_manager').setup({
      autoload_mode = require('session_manager.config').AutoloadMode.Disabled,
    })
  end
}
