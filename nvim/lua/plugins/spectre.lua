return {
  'windwp/nvim-spectre',
  event = 'VimEnter',
  keys = {
    { '<leader>sr',
      function()
        require('spectre').open()
      end, desc = 'Replace in files (Spectre)'
    },
  },
}
