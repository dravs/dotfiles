return {
  'dr4vs/neon.nvim',
  -- dir ='~/Projects/neon.vim',
  -- 'rebelot/kanagawa.nvim',
  -- 'dr4vs/srcery-vim',
  -- 'tiagovla/tokyodark.nvim',
  -- 'catppuccin/nvim', 
  lazy = false,
  config = function()
    vim.cmd.colorscheme("neon")
    -- vim.cmd [[colorscheme tokyodark]]
    -- vim.cmd [[colorscheme kanagawa]]
    -- vim.cmd [[colorscheme srcery]]
    -- vim.cmd [[colorscheme catppuccin]]
  end,
}
