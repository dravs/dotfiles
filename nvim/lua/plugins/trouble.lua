return {
  'folke/trouble.nvim',
  event = 'VimEnter',
  config = function()
    require('trouble').setup({
      action_keys = {
        hover = {},
      },
    })
  end,
}
