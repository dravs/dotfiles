#!/bin/sh

if [ -z "$1" ]; then
  ps -U $UID --no-headers -o pid,command
else
  pid=$(printf "%d" $(echo -e "$1" | awk '{print $1}'))
  kill $pid
fi