#!/bin/sh

if [ -z "$1" ]; then
  locate home media
else
  xdg-open "$1" > /dev/null 2>&1 &
fi